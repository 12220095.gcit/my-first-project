const usernameE1 = document.querySelector('#username');
const emailE1 = document.querySelector('#email');
const passwordE1 = document.querySelector('#password');
const confirmPasswordE1 = document.querySelector('#confirmPassword');
const form = document.querySelector('#signup');

form.addEventListener('submit', function(e) {
    e.preventDefault(); //prrevent the form from submitting

    let isUsernameValid = checkUsername(),
        isEmailVaild = checkEmail(),
        isPasswordValid = checkPassword();
    //     isConfirmPasswordValid = checkConfirmPassword();
    
    let isFormValid = isUsernameValid && isEmailVaild && isPasswordValid;
    // && isConfirmPasswordValid;
    //submit to the server if the form is valid
    
if (isFormValid) {
            alert('Submission Successful')
        }
    }
);


const checkUsername = () => {
    let valid = false;
    const min = 3,
    max = 25;
    const username = usernameE1.value.trim();

    if(!isRquired(username)) {
        showError(usernameE1,'username cannot be blank.');
    }
    else if (!isBetween(username.length, min, max)) {
        showError(usernameE1, `username must be between ${min} and ${max} characters.`)
    }
    else {
        showSuccess(usernameE1)
        valid = true;
    }

    return valid;
};

const isRquired = value => value=== '' ? false : true;
const isBetween = (lenght, min, max) => lenght < min || lenght > max ? false: true;

const showError = (input, message) => {
    const formField = input.parentElement;

    formField.classList.remove('success');
    formField.classList.add('error');

    const error = formField.querySelector('small')
    error.textContent = message;
};

const showSuccess = (input, message) => {
    const formField = input.parentElement;

    formField.classList.remove('error');
    formField.classList.add('success'); 
    
    const success = formField.querySelector('small')
    success.textContent = '';  

};

const checkEmail = () => {
    let valid = false;
   
    const email = emailE1.value.trim();

    if(!isRquired(email)) {
        showError(emailE1,'username cannot be blank.');
    }
    else if (!isEmailVaild(email)) {
        showError(emailE1, `Email is not valid.`)
    }
    else {
        showSuccess(emailE1)
        valid = true;
    }

    return valid;
};

const isEmailVaild = (email) => {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return re.test(email);
};

const checkPassword = () => {
    let valid = false;
   
    const password = passwordE1.value.trim();

    if(!isRquired(password)) {
        showError(passwordE1,'Password cannot be blank.');
    }
    else if (!isPasswordSecure(password)) {
        showError(passwordE1, `Password must has atleast 8 characters that include atleast one lowercase, one uppercase, one speical letter and minuium of 8 letter`)
    }
    else {
        showSuccess(passwordE1)
        valid = true;
    }
    return valid;
};

const isPasswordSecure = (password) => {
    const re = new
     RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");

    return re.test(password)
}

const checkConfirmPassword = () => {
    let valid = false;
   
    const password = confirmPasswordE1.value.trim();

    if(!isRquired(password)) {
        showError(confirmPasswordE1,'Confirm Password cannot be blank.');
    }
    else if (password == !passwordE1) {
        showError(confirmPasswordE1, `Password is not Matched`)
    }
    else {
        showSuccess(confirmPasswordE1)
        valid = true;
    }

    return valid;
};

